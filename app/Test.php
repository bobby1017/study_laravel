<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Test extends Model {

	//需要实现自动提交的字段，使用seeder必须要填写的
    protected $fillable = [
        'title',
        'body',
        'published_at'
    ];
    protected $dates = ['published_at'];
    //公共自动处理的方法  set+字段名+Attribute
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d',$date);
    }
    //公共处理的方法  scope+自定义的方法名字
    public function scopePublished($query)
    {
        $query->where('published_at','<=',Carbon::now());
    }

}
