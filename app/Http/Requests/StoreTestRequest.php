<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreTestRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        //表单验证规则
		return [
            'title' => 'required',
            'body' => 'required'
			//
		];
	}

}
