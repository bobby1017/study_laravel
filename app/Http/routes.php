<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('test/show/{id}','TestController@show');
Route::get('test/index/{id?}','TestController@index', function($id=null)
{
//    return 'TestController@index';
});
Route::get('test/create','TestController@create');
Route::post('test/store','TestController@store');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function()
{
    Route::get('/', 'AdminHomeController@index');
    Route::get('show/{id}','AdminHomeController@show');
    Route::resource('pages', 'PagesController');
});