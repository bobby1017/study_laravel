<?php
/**
 * Created by PhpStorm.
 * User: fanchenjueshi
 * Date: 15/9/29
 * Time: 08:33
 */
?>
@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">新增 Page</div>

                    <div class="panel-body">
                        {!!Form::open(['url'=>'test/store'])!!}
                        <div class="form-group">
                            {!! Form::label('title','标题:') !!}
                            {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'标题']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('body','正文:') !!}
                            {!! Form::textarea('body',null,['class'=>'form-control','placeholder'=>'正文']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('published_at','发布日期') !!}
                            {!! Form::input('date','published_at',date('Y-m-d'),['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('新增pages',['class'=>'btn btn-success form-control']) !!}
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
                @if($errors->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection